@extends("layouts/landing")
@section("content")

    <div class ="align-row shipped-qstn">
        <strong>Have my product shipped yet?</strong>
        <!--publishsection*****-->
        <div class="pos-abs-2">
            <a class="mg-color" href="">
                <i class="fa fa-refresh" aria-hidden="true"></i>
                Revert
            </a>
            <a class="mg-color-1" href="">
                <i class="fa fa-trash" aria-hidden="true"></i>
                Delete
            </a>
            <a  class="green-btn"style="color:#ffffff;  margin-right: 15px" href="">
                <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                <strong>Publish</strong>
            </a>
        </div>
        <!--*****publishsection*****-->
    </div>


    <div class="main-block">
        <!--*****TrainingSection*****-->
        <div class =" txt-left">
            <strong style=" font-size: 20px">Training</strong>
            <br>
            <p style="color: #616261; font-size: 20px">Add answers to your bot</p>
            <div class="meter-block"></div>
        <div>
        <!--*****Training   Section*****-->

        <!--*****QstnAns section*****-->
        <div  class="box1">
            <div class="align-row" style=" height: 100px">
                <div class ="small-section  small-section-border-radius">
                    <i class="fa fa-lightbulb-o fa-2x padding-color-3"aria-hidden="true"></i>
                </div>
                <div class="align-row question-box-1">
                    <div class="padding-50-35">
                        <strong class=" clr-2">Type a question to train to this answer..</strong>
                    </div>
                    <div class="pos-abs-3">
                        <i class="fa fa-plus-circle fa-2x" style=" color:#2766cd " aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="align-row">
                <div class="icon-style">
                    <i class="fa fa-commenting fa-2x padding-35-40" aria-hidden="true"></i>
                </div>
                <div>
                    <div class=" align-row section pos-rel">
                        <div class="padding-50-35">
                            <strong class="clr-black">Have my product shipped yet?</strong>
                        </div>
                        <div class="pos-abs-3">
                            <i class="fa fa-minus-circle fa-2x clr-1"  aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="align-row section pos-rel">
                        <div class="padding-50-35">
                            <strong class="clr-black">Packaged?</strong>
                        </div>
                        <div class="pos-abs-3">
                            <i class="fa fa-minus-circle fa-2x clr-1"aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                </div>
            </div> 
        </div> 
        <!--*****QstnAns section*****-->
            


        <!--*****Msg&Action*****-->
        <div style="margin-top: 30px">
            <a class="txt-left">
            <strong style=" font-size: 20px">Message and Action</strong>
            <br>
            <p style="color: #616261; font-size: 20px">Drag an item to reodrder its position in the order</p>
            </a>
        </div>
        <!--*****Msg&Action*****-->

        <!--*****EnglishButton*****-->
        <div class="pos-rel">
            <div class="align-row pos-abs-1">
                <a class="lang-btn clr-black" href="" >
                <i class="fa fa-globe" aria-hidden="true"></i>
                English</a>
                <a class="arrow-btn">
                <i class="fa fa-angle-double-down fa-1x"aria-hidden="true"></i>            
                </a>
            </div>
        </div> 
        <!--*****EnglishButton*****--> 

        <!--****Amazon****-->   
        <div  class="box">
            <div class="align-row">
                <div class ="small-section small-section-border-radius-1 bg-clr-black">
                    <i class="fab fa-amazon fa-3x padding-color-2"aria-hidden="true"></i>
                </div>
                <div class="align-row amazon-box">
                    <div class="padding-40-35">
                    <i class="fas fa-bars fa-2x"></i>
                        <strong class="padding-color-5">  Amazon - Product Lookup</strong>
                    </div>
                    <div class="pos-abs-3">
                        <i class="fa fa-trash fa-2x clr-1" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div> 
        <!--****Amazon****-->       
        
        <!--*****AddInAction******-->
        <div class = "align-row mg-top-lft">
            <a href="" class="action-btn txt-center clr-white"><strong>Add in Action</strong></a>
            <div class="complete-action-btn-fancy">
                <a class="action-btn-fancy mg-color-3" href="" >
                    <i class="fa fa-random" aria-hidden="true"></i></a>
                <a class="action-btn-fancy mg-color-3" href="">
                    <i class="fa fa-random" aria-hidden="true"></i></a>
                <a class="action-btn-fancy mg-color-3" href="">
                    <i class="fa fa-plug" aria-hidden="true"></i></a>
                <a class="action-btn-fancy mg-color-3" href="">
                    <i class="fa fa-commenting" aria-hidden="true"></i></a>
                <a class="action-btn-fancy mg-color-3" href="">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i></a>
            </div>
        </div>
        <!--*****AddInAction******-->

        <!--*****CaptureMsg*****-->
        <div  class="box1 bg-clr-2 h-400">
            <div class=" align-row">
                <!--*****MsgToolbar*****-->
                <div class ="small-section small-section-border-radius bg-clr" >
                    <i class="fa fa-commenting fa-2x callout-section" aria-hidden="true"></i>
                </div>
                <div class="align-row capture-box">
                    <div class="padding-40-35">
                        <i class="fas fa-bars fa-2x"></i>
                            <strong class="padding-color-6">  Capture Message</strong>
                    </div>
                    <div class=" pos-abs-3">
                        <i class="fa fa-trash fa-2x clr-1" aria-hidden="true"></i>
                    </div>
                </div>
                <!--*****MsgToolbar*****-->
            </div>
            <div class="align-row">
                <div class="callout-box-1">
                <i class="fa fa-commenting fa-2x callout-section-1 padding-15-15" aria-hidden="true"></i>
                </div>
                <div class=" section-big">
                    <div class="padding-50-20">
                        <strong class="clr-black">Have my product shipped yet?</strong>
                        <div class='inner-box-1'>Whats on your mind?
                        </div>
                    </div>
                </div>
            </div>
            <div class="align-row">
                <div class="callout-box-2">
                    
                    <i class="fa fa-flask fa-2x callout-section padding-16-16" aria-hidden="true"></i>
                </div>
                <!--*****txtBoxes*****-->
                <div class=" align-row section-big pos-rel ">
                    <div class="padding-border">
                        <strong class="clr-black">Variable</strong>
                        <div class="local-box">Local
                        </div>
                    </div>
                    <div class="padding-border">
                        <strong class="clr-black">Type</strong>
                        <div class="local-box">Local
                        </div>
                    </div>
                    <div class="padding-border">
                        <strong class="clr-black">Validation</strong>
                        <div class="local-box">Local
                        </div>
                    </div>
                    <div class="pos-abs-6">
                        <i class="fa fa-plus-circle fa-2x call clr-1"aria-hidden="true"></i>
                    </div>
                </div>
                <!--*****txtBoxes*****-->
            </div>
        </div>
        <!--*****CaptureMsg*****-->
        <!--******TextMsg section******-->
        <div  class="box2 ">
            <div class=" align-row">
                <div class ="small-section small-section-border-radius bg-clr" >
                    <i class="fa fa-pencil-square fa-3x padding-color-1" aria-hidden="true"></i>
                </div>
                <div class="align-row txt-msg-blk">
                    <div class="padding-40-35">
                        <i class="fas fa-bars fa-2x"></i>
                            <strong class="padding-color-7">  Text Messages</strong>
                    </div>
                    <div class="pos-abs-3">
                        <i class="fa fa-trash fa-2x clr-1" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class=" section-big-new padding-border-1">
                <strong class="clr-black">Variable</strong>
                <div  class=" local-box-1">Local
                </div>
                <div style=" margin-top: 30px; ">
                    <strong class="clr-black">Insert invoice id customer name</strong>
                </div>
            
            <div class="mg-top-lft-1 ">
                <strong class=" clr-black">Conditions</strong>
            </div>
        </div> 

        <!--******TextMsg section******-->
            
        <!--******Invoice section******-->
        <div  class="box3" style="margin-top: 150px">
            <div class="text-style bg-clr"><strong>If</strong></div>
                <div style=" width: 100%; height:229px;">
                    <div class="align-row"> 
                        <div style=" margin-top: 30px; margin-left: 30px">
                            <strong>billing:invoice_id - true<strong>
                        </div>
                        <div class=" customer-box">
                            <strong>
                            Thanks customer: name your product has shipped yesterday</strong></div>
                        </div>
                        <div class="align-row" style=" padding-top: 32px"> 
                            <div style=" margin-top: 30px; margin-left: 30px">
                                <strong>billing:invoice_id - false<strong>
                            </div>
                            <div class="false-box">
                                <strong>
                                It seems your prooduct haven't shipped</strong>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
        <!--******Invoice section******-->
    </div>


@endsection