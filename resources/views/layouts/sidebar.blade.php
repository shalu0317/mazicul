
<div class="side-bar-block " style=" z-index: 100" >
        <!--*****Searchbar*****-->
        <div class="search-bar-main">
            <div class ="search-bar-inside">
                <i class="fa fa-search" style="padding-left: 10px"aria-hidden="true"></i>
                <a class="font-padding">when:today</a>
            </div>
            <div class="plus-icon">
                <i class="fa fa-plus-square fa-2x padding-color-4" aria-hidden="true"></i>
            </div>
        </div>
        <!--*****Searchbar*****-->
        <!--*****Recent*****-->
        <div style="height: 70px">
            <div class="recent-pos">
                <a class="font-padding">Recent</a>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </div>
        </div>
        <!--*****Recent*****-->
        <div class ="align-row" style="height: 120px">
            <div class="header-bar">
                <i class="fa fa-circle-o fa-2x" style =" color: #f47f09" aria-hidden="true"></i>
                <div class="header-bar-txt">
                    <strong style="font-size: 16px">Title</strong>
                    <br>
                    <p style="color: #8a8c8e">{{ $details[0]->title }}</p>
                </div>
            </div>
        </div>
        <div class ="align-row" style="height: 120px">
            <div class="header-bar">
                <i class="fa fa-circle-o fa-2x" style =" color: #f47f09" aria-hidden="true"></i>
                <div style="position: absolute; left: 45px; top: 22px; width: 250px">
                    <strong style="font-size: 16px">Description</strong>
                    <p style="color: #8a8c8e">{{ $details[0]->description }}</p>
                </div>
            </div>
        </div>
        <div class ="align-row" style="height: 120px">
            <div class="header-bar">
                <i class="fa fa-circle-o fa-2x" style =" color: #f47f09" aria-hidden="true"></i>
                <div class="header-bar-txt">
                    <strong style="font-size: 16px">Slug<br></strong>
                    <strong class="font-color"></strong>
                    <p style="color: #8a8c8e">{{ $details[0]->slugs }}</p>
                </div>
            </div>
        </div>
        <!--*****UnpublishedTab*****-->
        <div style="height: 70px">
            <div class="unpublished-style">
            
            <a style="padding-left:25px; font-size: 18px">Unpublished</a>
            <i class="fa fa-angle-down" aria-hidden="true"></i>

            </div>
        </div>
        <!--*****UnpublishedTab*****-->
    <div class ="align-row" style="height: 120px">
        <div class="header-bar">
            <i class="fa fa-circle-o fa-2x" style =" color: #f47f09" aria-hidden="true"></i>
            <div class="header-bar-txt">
                <strong style="font-size: 16px">Handoff to support?<br></strong>
                <strong style="font-size: 16px; color: #8a8c8e">Support</strong>
                <p style="color: #8a8c8e">For when someone need help from your human support team.</p>
            </div>
        </div>
    </div>
</div>

