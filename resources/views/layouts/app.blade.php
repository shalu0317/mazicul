<!DOCTYPE html>
<html>
    <head>
        <title>BetaLance Demo</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="style.css" rel="stylesheet">

        <!-- Styles -->
    </head>
    <body>
        
            <form action="/display" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                Title <input type="text" name ="title" placeholder ="enter title">
                Description <input type="text" name="description" placeholder="enter description">
                Add data <input type="submit">
            </form>

    </body>
</html>
