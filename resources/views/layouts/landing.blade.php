<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="/style.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/fontawesome.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/fontawesome.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/all.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/all.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/brands.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/brands.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/regular.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/regular.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/solid.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/solid.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/svg-with-js.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/svg-with-js.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/v4-shims.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/v4-shims.min.css">
        <title>Mazicul</title>

        <!-- Fonts -->
        
        <link href="/https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="/bootstrapcss/bootstrap.css" rel="stylesheet" type="text/css">
       <script src="/js/bootstrap1.js"></script>
        <!-- Styles -->
    
    </head>
    <body class="bg-clr-2">

<!--*****Toolbar*****-->
<div class="toolbar align-row  pos-rel">
    <div class="padding-20-30">
        <a class="margin-txt-color toolbar-btn" href="">
            <i class="fa fa-users" aria-hidden="true"></i>
        Answers</a>
        <a class="margin-txt-color toolbar-btn" href="/question">
            <i class="fa fa-commenting" aria-hidden="true"></i>
        Questions</a>
        <a class="margin-txt-color toolbar-btn" href="">
            <i class="fa fa-comments" aria-hidden="true"></i>
        Conversations</a>
        <a class="margin-txt-color toolbar-btn" href="">
            <i class="fa fa-pie-chart" aria-hidden="true"></i>
        Analytics</a>
        <a class="margin-txt-color toolbar-btn" href="">
            <i class="fa fa-cog" aria-hidden="true"></i>
        Settings</a>
    </div>
    <div class=" min-box">
        <div class="align-row ">
            <div class=  "padding-30-20">
                <a class="txt-box toolbar-btn" href="">
                Test Box <i class="fa fa-square" aria-hidden="true"></i>
                </a>
            </div>
                <a class="rounded-square txt-center"></a>
                <a class="color-padding">
                <strong style="color: #ffffff; opacity: 100%">Kasper</strong>
                <br> <text style="color: #ffffff; opacity: 70%">Agent</text></a>
            <div class="padding-25-40">
                <a class="dull-clr-1">
                <i class="fa fa-ellipsis-v fa-2x clr-white"  aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
<!--*****Toolbar*****-->

<div class="align-row bg-clr-3 h-auto">
    <!--***sidebar***-->
        @include("layouts.sidebar")
    <!--***sidebar***-->
    <div class="pos-rel-1">
        @yield("content")
    </div>
</div>
</body>
</html>
