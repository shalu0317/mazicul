<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="/style.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/fontawesome.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/fontawesome.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/all.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/all.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/brands.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/brands.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/regular.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/regular.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/solid.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/solid.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/svg-with-js.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/svg-with-js.min.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/v4-shims.css">
        <link rel="stylesheet" href="/css/fonts/fontawesome/cssFile/v4-shims.min.css">
        <title>Mazicul</title>

        <!-- Fonts -->
        
        <link href="/https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="/bootstrapcss/bootstrap.css" rel="stylesheet" type="text/css">

       <script src="/js/bootstrap1.js"></script>
        <!-- Styles -->
    
    </head>
    <body class="bg-clr-2">

<!--*****Toolbar*****-->
<div class="toolbar align-row  pos-rel">
    <div class="padding-20-30">
        <a class="margin-txt-color toolbar-btn" href="">
            <i class="fa fa-users" aria-hidden="true"></i>
        Answers</a>
        <a class="margin-txt-color toolbar-btn" href="/question">
            <i class="fa fa-commenting" aria-hidden="true"></i>
        Questions</a>
        <a class="margin-txt-color toolbar-btn" href="">
            <i class="fa fa-comments" aria-hidden="true"></i>
        Conversations</a>
        <a class="margin-txt-color toolbar-btn" href="">
            <i class="fa fa-pie-chart" aria-hidden="true"></i>
        Analytics</a>
        <a class="margin-txt-color toolbar-btn" href="">
            <i class="fa fa-cog" aria-hidden="true"></i>
        Settings</a>
    </div>
    <div class=" min-box">
        <div class="align-row ">
            <div class=  "padding-30-20">
                <a class="txt-box toolbar-btn" href="">
                Test Box <i class="fa fa-square" aria-hidden="true"></i>
                </a>
            </div>
                <a class="rounded-square txt-center"></a>
                <a class="color-padding">
                <strong style="color: #ffffff; opacity: 100%">Kasper</strong>
                <br> <text style="color: #ffffff; opacity: 70%">Agent</text></a>
            <div class="padding-25-40">
                <a class="dull-clr-1">
                <i class="fa fa-ellipsis-v fa-2x clr-white"  aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
<!--*****Toolbar*****-->

<div class="align-row bg-clr-3 h-auto ">
    <!--***sidebar***-->
    @include("layouts/sidebar")
    <!--***sidebar***-->

    <div class="pos-rel-1">
        <div class ="align-row shipped-qstn">
            <strong>Have my product shipped yet?</strong>
            <!--publishsection*****-->
            <div class="pos-abs-2">
                <a class="mg-color" href="">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                    Revert
                </a>
                <a class="mg-color-1" href="">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    Delete
                </a>
                <a  class="green-btn"style="color:#ffffff;  margin-right: 15px" href="">
                    <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    <strong>Publish</strong>
                </a>
            </div>
            <!--*****publishsection*****-->
        </div>


        <div class="main-block">
            <!--*****TrainingSection*****-->
            <div class =" txt-left">
                <strong style=" font-size: 20px">Training</strong>
                <br>
                <p style="color: #616261; font-size: 20px">Add answers to your bot</p>
                <div class="meter-block"></div>
            <div>
            <!--*****Training   Section*****-->

            <!--*****QstnAns section*****-->
            <div  class="box1">
                <div class="align-row" style=" height: 100px">
                    <div class ="small-section  small-section-border-radius">
                        <i class="fa fa-lightbulb-o fa-2x padding-color-3"aria-hidden="true"></i>
                    </div>
                    <div class="align-row question-box-1">
                        <div class="padding-50-35">
                            <strong class=" clr-2">Type a question to train to this answer..</strong>
                        </div>
                        <div class="pos-abs-3">
                            <i class="fa fa-plus-circle fa-2x" style=" color:#2766cd " aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="align-row">
                    <div class="icon-style">
                        <i class="fa fa-commenting fa-2x padding-35-40" aria-hidden="true"></i>
                    </div>
                    <div>
                        <div class=" align-row section pos-rel">
                            <div class="padding-50-35">
                                <strong class="clr-black">Have my product shipped yet?</strong>
                            </div>
                            <div class="pos-abs-3">
                                <i class="fa fa-minus-circle fa-2x clr-1"  aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="align-row section pos-rel">
                            <div class="padding-50-35">
                                <strong class="clr-black">Packaged?</strong>
                            </div>
                            <div class="pos-abs-3">
                                <i class="fa fa-minus-circle fa-2x clr-1"aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    </div>
                </div> 
            </div> 
            <!--*****QstnAns section*****-->
                


            <!--*****Msg&Action*****-->
            <div style="margin-top: 30px">
                <a class="txt-left">
                <strong style=" font-size: 20px">Message and Action</strong>
                <br>
                <p style="color: #616261; font-size: 20px">Drag an item to reodrder its position in the order</p>
                </a>
            </div>
            <!--*****Msg&Action*****-->

            <!--*****EnglishButton*****-->
            <div class="pos-rel">
                <div class="align-row pos-abs-1">
                    <a class="lang-btn clr-black" href="" >
                    <i class="fa fa-globe" aria-hidden="true"></i>
                    English</a>
                    <a class="arrow-btn">
                    <i class="fa fa-angle-double-down fa-1x"aria-hidden="true"></i>            
                    </a>
                </div>
            </div> 
            <!--*****EnglishButton*****--> 

            <!--****Amazon****-->   
            <div  class="box">
                <div class="align-row">
                    <div class ="small-section small-section-border-radius-1 bg-clr-black">
                        <i class="fab fa-amazon fa-3x padding-color-2"aria-hidden="true"></i>
                    </div>
                    <div class="align-row amazon-box">
                        <div class="padding-40-35">
                        <i class="fas fa-bars fa-2x"></i>
                            <strong class="padding-color-5">  Amazon - Product Lookup</strong>
                        </div>
                        <div class="pos-abs-3">
                            <i class="fa fa-trash fa-2x clr-1" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div> 
            <!--****Amazon****-->       
            
            <!--*****AddInAction******-->
            <div class = "align-row mg-top-lft">
                <a href="" class="action-btn txt-center clr-white"><strong>Add in Action</strong></a>
                <div class="complete-action-btn-fancy">
                    <a class="action-btn-fancy mg-color-3" href="" >
                        <i class="fa fa-random" aria-hidden="true"></i></a>
                    <a class="action-btn-fancy mg-color-3" href="">
                        <i class="fa fa-random" aria-hidden="true"></i></a>
                    <a class="action-btn-fancy mg-color-3" href="">
                        <i class="fa fa-plug" aria-hidden="true"></i></a>
                    <a class="action-btn-fancy mg-color-3" href="">
                        <i class="fa fa-commenting" aria-hidden="true"></i></a>
                    <a class="action-btn-fancy mg-color-3" href="">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i></a>
                </div>
            </div>
            <!--*****AddInAction******-->

            <!--*****CaptureMsg*****-->
            <div  class="box1 bg-clr-2 h-400">
                <div class=" align-row">
                    <!--*****MsgToolbar*****-->
                    <div class ="small-section small-section-border-radius bg-clr" >
                        <i class="fa fa-commenting fa-2x callout-section" aria-hidden="true"></i>
                    </div>
                    <div class="align-row capture-box">
                        <div class="padding-40-35">
                            <i class="fas fa-bars fa-2x"></i>
                                <strong class="padding-color-6">  Capture Message</strong>
                        </div>
                        <div class=" pos-abs-3">
                            <i class="fa fa-trash fa-2x clr-1" aria-hidden="true"></i>
                        </div>
                    </div>
                    <!--*****MsgToolbar*****-->
                </div>
                <div class="align-row">
                    <div class="callout-box-1">
                    <i class="fa fa-commenting fa-2x callout-section-1 padding-15-15" aria-hidden="true"></i>
                    </div>
                    <div class=" section-big">
                        <div class="padding-50-20">
                            <strong class="clr-black">Have my product shipped yet?</strong>
                            <div class='inner-box-1'>Whats on your mind?
                            </div>
                        </div>
                    </div>
                </div>
                <div class="align-row">
                    <div class="callout-box-2">
                        
                        <i class="fa fa-flask fa-2x callout-section padding-16-16" aria-hidden="true"></i>
                    </div>
                    <!--*****txtBoxes*****-->
                    <div class=" align-row section-big pos-rel ">
                        <div class="padding-border">
                            <strong class="clr-black">Variable</strong>
                            <div class="local-box">Local
                            </div>
                        </div>
                        <div class="padding-border">
                            <strong class="clr-black">Type</strong>
                            <div class="local-box">Local
                            </div>
                        </div>
                        <div class="padding-border">
                            <strong class="clr-black">Validation</strong>
                            <div class="local-box">Local
                            </div>
                        </div>
                        <div class="pos-abs-6">
                            <i class="fa fa-plus-circle fa-2x call clr-1"aria-hidden="true"></i>
                        </div>
                    </div>
                    <!--*****txtBoxes*****-->
                </div>
            </div>
            <!--*****CaptureMsg*****-->
            <!--******TextMsg section******-->
            <div  class="box2">
                <div class=" align-row">
                    <div class ="small-section small-section-border-radius bg-clr" >
                        <i class="fa fa-pencil-square fa-3x padding-color-1" aria-hidden="true"></i>
                    </div>
                    <div class="align-row txt-msg-blk">
                        <div class="padding-40-35">
                            <i class="fas fa-bars fa-2x"></i>
                                <strong class="padding-color-7">  Text Messages</strong>
                        </div>
                        <div class="pos-abs-3">
                            <i class="fa fa-trash fa-2x clr-1" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class=" section-big-new padding-border-1">
                    <strong class="clr-black">Variable</strong>
                    <div  class=" local-box-1">Local
                    </div>
                    <div style=" margin-top: 30px; ">
                        <strong class="clr-black">Insert invoice id customer name</strong>
                    </div>
                </div>
                <div class="mg-top-lft-1 ">
                    <strong class=" clr-black">Conditions</strong>
                </div>
              <div class=" align-row">
                    <div class ="small-section small-section-border-radius bg-clr" >
                        <i class="fa fa-pencil-square fa-3x padding-color-1" aria-hidden="true"></i>
                    </div>
                    <div class="align-row txt-msg-blk">
                        <div class="padding-40-35">
                            <i class="fas fa-bars fa-2x"></i>
                                <strong class="padding-color-7">  Text Messages</strong>
                        </div>
                        <div class="pos-abs-3">
                            <i class="fa fa-trash fa-2x clr-1" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class=" section-big-new padding-border-1">
                    <strong class="clr-black">Variable</strong>
                    <div  class=" local-box-1">Local
                    </div>
                    <div style=" margin-top: 30px; ">
                        <strong class="clr-black">Insert invoice id customer name</strong>
                    </div>
                </div>
                <div class="mg-top-lft-1 ">
                    <strong class=" clr-black">Conditions</strong>
                </div>
            </div> 

            <!--******TextMsg section******-->
                
            <!--******Invoice section******-->
            <div  class="box3">
                <div class="text-style bg-clr"><strong>If</strong></div>
                    <div style=" width: 100%; height:229px;">
                        <div class="align-row"> 
                            <div style=" margin-top: 30px; margin-left: 30px">
                                <strong>billing:invoice_id - true<strong>
                            </div>
                            <div class=" customer-box">
                                <strong>
                                Thanks customer: name your product has shipped yesterday</strong></div>
                            </div>
                            <div class="align-row" style=" padding-top: 32px"> 
                                <div style=" margin-top: 30px; margin-left: 30px">
                                    <strong>billing:invoice_id - false<strong>
                                </div>
                                <div class="false-box">
                                    <strong>
                                    It seems your prooduct haven't shipped</strong>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div> 
            <!--******Invoice section******-->
        </div>
    </div>
</div>
</body>
</html>
