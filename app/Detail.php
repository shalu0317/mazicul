<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    //
    protected $guard = 'detail';
    protected $table = 'details';

}
