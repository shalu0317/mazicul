<?php

;

namespace App\Http\Controllers;

use App\Detail;
use DB;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{ 

    public function display(Request $request)
    {
        return view('layouts.app');
    }




    public function saveDisplay(Request $request)

    { 
        $data['data']= $request->all(); 
        $details = new Detail;
        $details->title= $request->title;
        $details->description = $request->description;
        $details->slugs = Str::slug($request->title, '-');
        $details->save();
      
        return redirect()->route("/displayslug","$details->slugs");
    }

    public function displaySlug(Request $request,$slug)
    {
        $data = [];
        $data['details'] = Detail::where('slugs',$slug)->get();
        
            return redirect('toolpage/'. $slug)->with($data);
        
    }
    
}
