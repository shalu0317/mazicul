<?php

namespace App\Http\Controllers;

use App\Detail;

use App\UserSystemInfo;

use Illuminate\Http\Request;



class UserSystemInfoController extends Controller
{
    
    public function getUserIp() {

        $getip = UserSystemInfo::get_ip();

        echo "$getip";
    }

    public function getIp(Request $request) {

        $data = [];
        $data['details'] = Detail::where('slugs', 'like', 'ip-address')->get();

        $clientIp = $request->ip();
        

        return view('toolpage/ip-address')->with($data)->with('ip', $clientIp);
    } 

    public function getHost(Request $request) {
        $data = [];
        $data['details'] = Detail::where('slugs', 'like', 'get-hostname')->get();

        $host = $request->gethost();

        return view('toolpage/get-hostname')->with($data)->with('host', $host);       
    }
}
