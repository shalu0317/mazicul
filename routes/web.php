

<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/question', function() {
    return view('layouts.question_portal');
});



Route::get('/display', 'PostController@display');

Route::get('/display/{slug}', 'PostController@displaySlug')->name('/displayslug');

Route::get('/toolpage/ip-address', 'UserSystemInfoController@getip');

Route::get('/toolpage/get-hostname', 'UserSystemInfoController@getHost');

Route::post('/display', 'PostController@saveDisplay');